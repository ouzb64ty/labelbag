var canvas = document.getElementById("boxAnnotation");
var ctx = canvas.getContext('2d');
var firstX = 0;
var firstY = 0;
var pointer = false;
var img = new Image();
var areas = [];

/* Tools */

function getMousePos(canvas, e) {
    var rect = canvas.getBoundingClientRect();

    return {
      	x: e.clientX - rect.left,
      	y: e.clientY - rect.top
    };
}

function refresh() {
	ctx.font = "15px Arial";
	ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
	for (var i = 0; i < areas.length; i++)
		drawArea(areas[i]);
}

function drawArea(area) {
	var topCoords = [];

	if (area.coords[2] < 0) {
		topCoords.push(area.coords[0] + area.coords[2]);
		topCoords.push(area.coords[0]);
	} else {
		topCoords.push(area.coords[0]);
		topCoords.push(area.coords[0] + area.coords[2]);
	}
	if (area.coords[3] < 0) topCoords.push(area.coords[1] + area.coords[3]);
	else topCoords.push(area.coords[1]);
	if (area.name) {
		ctx.strokeText(area.name, topCoords[0], topCoords[2]);
		ctx.strokeText('x', topCoords[1] - 10, topCoords[2]);
	}
	ctx.strokeRect(area.coords[0], area.coords[1], area.coords[2], area.coords[3]);
}

function addArea(markName, x1, y1, x2, y2) {
	var area = {};

	area.name = markName;
	area.coords = [x1, y1, x2, y2];
	drawArea(area);
	areas.push(area);
}

/* On Mouse Move */

canvas.addEventListener('mousemove', function(e) {
	var pos = getMousePos(canvas, e);
	var tmpArea = {};

	if (pointer == true) {
		tmpArea.coords = [firstX, firstY, pos.x - firstX, pos.y - firstY];
		refresh();
		drawArea(tmpArea);
	}
});

/* On Mouse Down */

canvas.addEventListener('mousedown', function(e) {
	var pos = getMousePos(canvas, e);

	firstX = pos.x;
	firstY = pos.y;
	pointer = true;
});

/* On Mouse Up */

canvas.addEventListener('mouseup', function(e) {
	var pos = getMousePos(canvas, e);
	var markName;

	pointer = false;
    ctx.fillStyle = "#000000";
    markName = prompt("Nom de la zone");
    if (markName)
    	addArea(markName, firstX, firstY, pos.x - firstX, pos.y - firstY);
    else
    	alert("Veuillez nommer la zone");
    refresh();
});

/* On Load */

function draw() {
	img.onload = function() {
		canvas.width = img.width;
		canvas.height = img.height;
		refresh();
	}
	img.src = "https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
}